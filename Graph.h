#define LIMIT 100

#include <stdio.h>
#include <stdlib.h>

#include "./Matrix.h"
#include "./Queue.h"
#include "./Stack.h"
#include "./List.h"

typedef struct
{
    node **base;
    int len;
} AdjacencyList;

Matrix adjacency_matrix_from_file(FILE *fp)
{
    char line[LIMIT];
    fgets(line, LIMIT, fp);
    int len = atoi(line);

    Matrix tx = create_matrix(len, len);

    int i = 0, j;
    while (fgets(line, LIMIT, fp) != NULL)
    {
        char ch = line[0];
        j = 0;
        while (ch != '\0')
        {
            if (ch == '0')
                tx.base[i][j / 2] = 0;
            else if (ch == '1')
                tx.base[i][j / 2] = 1;
            ch = line[++j];
        }
        i++;
    }

    return tx;
}

AdjacencyList adjacency_list_from_file(FILE *fp)
{
    AdjacencyList list;

    char line[LIMIT];
    fgets(line, LIMIT, fp);
    int len = atoi(line);

    list.base = (node **)calloc(len, sizeof(node *));
    list.len = len;

    int i, j;
    for (i = 0; i < len; i++)
        list.base[i] = NULL;

    i = 0;
    while (fgets(line, LIMIT, fp) != NULL)
    {
        char ch = line[0];
        j = 0;
        while (ch != '\0')
        {
            if (ch == '1')
                list.base[i] = insert_last(list.base[i], j / 2);
            ch = line[++j];
        }
        i++;
    }

    return list;
}

void print_adjacency_list(AdjacencyList list)
{
    int i;
    for (i = 0; i < list.len; i++)
    {
        printf("(%d) -> ", i);
        display_list(list.base[i]);
    }
}

void BFS(AdjacencyList graph, int startVertex)
{
    int *visited = (int *)calloc(graph.len, sizeof(int));

    Queue q = create_queue(graph.len);
    enqueue(&q, startVertex);
    visited[startVertex] = 1;

    printf("---BFS START---\n");

    while (!is_queue_empty(q))
    {
        int v = dequeue(&q);
        printf("%d\n", v);

        node *ws = graph.base[v];
        while (ws != NULL)
        {
            int w = ws->data;
            if (!visited[w])
            {
                enqueue(&q, w);
                visited[w] = 1;
            }

            ws = ws->link;
        }
    }

    printf("---BFS END---\n");
}

void DFS(AdjacencyList graph, int startVertex)
{
    int *visited = (int *)calloc(graph.len, sizeof(int));

    Stack s = create_stack(graph.len);
    push(&s, startVertex);
    visited[startVertex] = 1;

    printf("---DFS START---\n");

    while (!is_stack_empty(s))
    {
        int v = pop(&s);
        printf("%d\n", v);

        node *ws = graph.base[v];
        while (ws != NULL)
        {
            int w = ws->data;
            if (!visited[w])
            {
                push(&s, w);
                visited[w] = 1;
            }

            ws = ws->link;
        }
    }

    printf("---DFS END---\n");
}