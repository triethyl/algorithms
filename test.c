#include <stdio.h>
#include <stdlib.h>

#include "./Graph.h"

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("Usage: %s [file]\n", argv[0]);
        exit(1);
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL)
    {
        printf("File not found.\n");
        exit(1);
    }

    AdjacencyList graph = adjacency_list_from_file(fp);

    int startVertex;
    printf("? Start At: ");
    scanf("%d", &startVertex);

    fclose(fp);
}